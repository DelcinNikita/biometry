Трекинг лиц с помощью Yolov5 + Deep Sort

Установка:

1) git clone --recurse-submodules https://gitlab.com/DelcinNikita/biometry.git

Если вы уже клонировали и забыли использовать --recurse-submodules, вы можете запустить git submodule update --init

2) docker-compose up

3) Перейдите по адресу http://localhost:5000/

ВАЖНО!

Перед тем, как загружать следующее видео, нужно удалить предыдущее. Для этого на странице с видео (на нее можно перейти, нажав на ссылку "video" на главной странице) нажмите на кнопку "delete".